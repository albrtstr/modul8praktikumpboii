/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author ASUS
 */
public class MasyarakatSekitar extends Penduduk{
    private String nomor;

    public MasyarakatSekitar(String nomor){
        this.nomor = nomor;
    }
    
    public MasyarakatSekitar() {
    }
    
    public MasyarakatSekitar(String dataNis, String dataNama, String dataTempatTanggalLahir){
        
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }
    
    @Override
    public double hitungIuran(){
        double value = Double.parseDouble(nomor);
        double iuran = value*100;
        return iuran;
    }
    
}
