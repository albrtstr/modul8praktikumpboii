/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_latihan;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author ASUS
 */
public class ReadDataDemo {
    
    public ReadDataDemo(){
        
    }
    
    public static void main(String[] args) throws FileNotFoundException, IOException {
        File inputFile = new File("latihan.txt");
        try {
            FileInputStream in = new FileInputStream(inputFile);
            String testInput = new String();
            int check;
            while ((check = in.read()) != -1) {
                testInput += (char) check;
            }
            in.close();
            System.out.println(testInput);
        } catch (IOException ex){
            System.out.println(ex.getMessage());
        }
    }
}
