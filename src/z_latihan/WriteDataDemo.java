/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z_latihan;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author ASUS
 */
public class WriteDataDemo {

    public WriteDataDemo() {

    }
    
    public static void main(String[] args) throws FileNotFoundException, IOException {
        File outputFile = new File("latihan.txt");
        FileOutputStream out = new FileOutputStream(outputFile);
        String testOutput = "This is an exercise to write data to a file with "
                + "FileOutputStream";
        
        //cara 1
        for (int i = 0; i < testOutput.length(); i++) {
            out.write((byte)testOutput.charAt(i));
        }
        
        //cara 2
//        out.write(testOutput.getBytes());
//        out.close();
    }
}
